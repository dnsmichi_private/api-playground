#!/usr/bin/env perl

use strict;
use warnings;

use GitLab::API::v4;
use Data::Dumper;
use Try::Tiny;
use feature qw(switch say);

my $project_id= 15730252; # this project
my $gitlab_api_url = 'https://gitlab.com/api/v4';
my $gitlab_api_token = $ENV{'GITLAB_COM_PRIVATE_TOKEN'};

my $api = GitLab::API::v4->new(
    url           => $gitlab_api_url,
    private_token => $gitlab_api_token,
);
my %params;

my $branches = $api->branches( $project_id );
#say Dumper($branches);

say "======== Commits ========";

my $commits = $api->commits(
    $project_id,
    \%params,
);

my @coll_commits;

foreach(@{$commits}) {
  my $c = $_;
  push @coll_commits, $c->{'title'};
}
say join("\n", @coll_commits);

say "======== Paginated Commits ========";
my $history = [];
my $gitLabHistory = $api->paginator('commits', $project_id, \%params);

if ($gitLabHistory->isa('GitLab::API::v4::Paginator')) {
    PAGE: while(1) {
        my $commits = [];
        try {
            $commits = $gitLabHistory->next_page();
        } catch {
            $commits = [];

            if ($_) {
                print "Can't get history: $_\n";
            }
        };

        last PAGE unless (ref $commits eq 'ARRAY' && @{$commits});

        push @{$history}, @{$commits};
    }
}

my @history_short_log;

foreach(@{$history}) {
  my $c = $_;
  push @history_short_log, $c->{'title'};
}
say join("\n", @history_short_log);
