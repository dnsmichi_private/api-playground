# GitLab Python API Client Playground

## Installation

```
pip install python-gitlab
```

```
vim $HOME/.python-gitlab.cfg

[global]
default = gitlab.com

[gitlab.com]
url = https://gitlab.com
private_token = xxx
api_version = 4
ssl_verify = true
```

## Queries

Modify `query.py` and/or run it.
