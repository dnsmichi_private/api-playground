#!/usr/bin/env python

import gitlab

PROJECT_ID=15013654

gl = gitlab.Gitlab.from_config('gitlab.com', ['/Users/michi/.python-gitlab.cfg'])
project = gl.projects.get(PROJECT_ID)

# https://python-gitlab.readthedocs.io/en/stable/gl_objects/protected_branches.html
print("Protected branches")
p_branches = project.protectedbranches.list()
print(p_branches)

print("Protected branch: master")
p_branch = project.protectedbranches.get('master')
print(p_branch)

protect_me = "*-support"
p = None
# https://python-gitlab.readthedocs.io/en/stable/gl_objects/access_requests.html?highlight=developer_access#access-requests
try:
    p = project.protectedbranches.get(protect_me)
except:
    pass

if not p:
    p = project.protectedbranches.create({
        'name': protect_me,
        #'merge_access_level': 40,
        #'push_access_level': 40
        'merge_access_level': 0,
        'push_access_level': 0
    })
print(p)


