#!/usr/bin/env python

import gitlab

PROJECT_ID=15013654

gl = gitlab.Gitlab.from_config('gitlab.com', ['/Users/michi/.python-gitlab.cfg'])

print("Project attributes required for creation")
print(gl.projects.get_create_attrs())

project = gl.projects.get(PROJECT_ID)
print("All project attributes")
print(project.attributes)

print("Project attributes matching access_level")
for a in project.attributes:
    if "access_level" in a:
        print(a)
