# API Playground

GitLab API playground with code samples for various languages.

- [python/](python/README.md)
- [perl/](perl/README.md)

